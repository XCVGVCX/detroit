package com.xcvgsystems.detroit;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.xcvgsystems.hypergiant.EVars;
import com.xcvgsystems.hypergiant.GVars;
import com.xcvgsystems.hypergiant.managers.MusicManager;
import com.xcvgsystems.hypergiant.managers.SceneManager;
import com.xcvgsystems.hypergiant.managers.TextureManager;
import com.xcvgsystems.hypergiant.scenes.ImageScene;
import com.xcvgsystems.hypergiant.scenes.WorldScene;
import com.xcvgsystems.hypergiant.utmf.*;

/**
 * Game-specific resource loader and setup, runs immediately after engine init and before game start.
 * @author Chris
 *
 */
public class DetroitLoader {
	
	public static void init()
	{
		
		//THIS LOADING STUFF IS ALL VERY TEMPORARY
		FileHandle gamePath = Gdx.files.internal(GVars.GAME_PATH);
		FileHandle texturesPath = gamePath.child("textures");
		FileHandle spritesPath = gamePath.child("sprites");
		FileHandle mapsPath = gamePath.child("maps");
		FileHandle musicPath = gamePath.child("music");
		
		//new code
		TextureManager.load("badlogic", texturesPath.child("badlogic.jpg"));
		TextureManager.load("books", texturesPath.child("bookshelf.png"));
		TextureManager.load("CEMENT1", texturesPath.child("CEMENT1.png"));
		TextureManager.load("GRASS1", texturesPath.child("GRASS1.png"));
		TextureManager.load("WALL1", texturesPath.child("WALL1.png"));
		TextureManager.loadAll(spritesPath);
		
		//
		MusicManager.load("D_RUNNIN", musicPath.child("D_RUNNIN.ogg"));
		
		//load some stuff
		SceneManager.addScene("TestScene2", new ImageScene("hypergiant", null, "MAP02"));
		
		UTMFMap map01 = UTMFLoader.loadMap(mapsPath.child("MAP01.utmf").readString());
		
		SceneManager.addScene("TestScene", new WorldScene(map01));
		SceneManager.changeScene("TestScene");
	}

}
