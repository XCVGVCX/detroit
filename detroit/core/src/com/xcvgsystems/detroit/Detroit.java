package com.xcvgsystems.detroit;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;
import com.xcvgsystems.hypergiant.*;
import com.xcvgsystems.hypergiant.managers.*;
import com.xcvgsystems.hypergiant.scenes.*;

public class Detroit extends ApplicationAdapter {
	
	private static final long TICK_DELAY = 33;
	private long time;
	//private FPSLogger logger;

	@Override
	public void create() {
		// display a temp logo
		/*
		SpriteBatch batch = new SpriteBatch();
		batch.begin();
		batch.draw(new Texture(Gdx.files.internal("startup.png")), 0f, 0f); //,Gdx.graphics.getWidth(), Gdx.graphics.getHeight()
		batch.end();
		batch.dispose();
		batch = null;
		*/
		
		// init engine
		Engine.init();
		
		//init game-specific code
		DetroitLoader.init();
		
		time = TimeUtils.millis();
		
		//logger = new FPSLogger();
		
		SceneManager.setDrawing(true);
		SceneManager.setRunning(true);
	}

	@Override
	public void render() {

		// loop here(?!)
		if(EVars.DEBUG)
			Engine.parseConsole();
		
		InputManager.poll();
		
		//30TPS tickrate limiting hopefully
		if(TimeUtils.timeSinceMillis(time) > TICK_DELAY)
		{
			time = TimeUtils.millis();
			
			//System.err.println("TICK");
			SceneManager.tick();
			
		}
				
		SceneManager.draw();
		
		//System.err.println("DRAW");
		//logger.log();

	}

	@Override
	public void dispose() {
		// dispose of all the native resources

		Engine.dispose();
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
