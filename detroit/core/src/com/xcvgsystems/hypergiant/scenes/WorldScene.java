package com.xcvgsystems.hypergiant.scenes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.xcvgsystems.hypergiant.exceptions.ThinkerNotFoundException;
import com.xcvgsystems.hypergiant.managers.*;
import com.xcvgsystems.hypergiant.thinkers.*;
import com.xcvgsystems.hypergiant.utmf.*;

public class WorldScene extends Scene {
	
	//TODO: more elegant and reconfigurable
	//TODO: comments, because I mean DAMN
	
	int TILE_SIZE = 32; //tile size in px: this is 32px by default, not sure where to define this
	int TILE_OFFSET_X = 0;
	int TILE_OFFSET_Y = 0;
	
	protected int width, height;
	protected String music;

	protected TextureRegion background;
	protected List<Sprite> layer0;
	protected List<Sprite> layer1;
	protected List<Sprite> layer3;
	
	protected List<Thinker> thinkers;
	PlayerPawn player;
	
	protected boolean[][] blockmap;
	
	public WorldScene(UTMFMap map)
	{
		camera= new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		//perform init steps
	
		loadMap(map);
	}
	
	private void loadMap(UTMFMap map)
	{
		//load map
		background = TextureManager.get(map.getBackground());
		
		//load blockmap
		blockmap = map.getBlockMap();
		width = map.getWidth();
		height = map.getHeight();
		music = map.getMusic();

		loadMapOffsets();
		
		loadMapTiles(map);
		
		loadMapObjects(map);
		
		player = new PlayerPawn(map.getStartX(), map.getStartY());
		thinkers.add(player);
	}
	
	private void loadMapOffsets()
	{
		//this is really shitty, but the math checks out, so :shrug:
		
		int pxWidth = width * TILE_SIZE;
		int pxHeight = height * TILE_SIZE;
		
		int pxXMargin = (Gdx.graphics.getWidth() - pxWidth) / 2;
		int pxYMargin = (Gdx.graphics.getHeight() - pxHeight) / 2;
		
		TILE_OFFSET_X += pxXMargin;
		TILE_OFFSET_Y += pxYMargin;
	}

	private void loadMapTiles(UTMFMap map) {
		//load map tiles (convert to Sprites)
		layer0 = new ArrayList<Sprite>();
		for(UTMFTile tile : map.getTileList(0))
		{
			Sprite spr = new Sprite(TextureManager.get(tile.getImage()));
			//	tile.getX() * TILE_SIZE, tile.getY() * TILE_SIZE, TILE_SIZE, TILE_SIZE);
			spr.setBounds(TILE_OFFSET_X + tile.getX() * TILE_SIZE, TILE_OFFSET_Y + tile.getY() * TILE_SIZE, TILE_SIZE, TILE_SIZE);
			layer0.add(spr);
		}
		
		layer1 = new ArrayList<Sprite>();
		for(UTMFTile tile : map.getTileList(1))
		{
			Sprite spr = new Sprite(TextureManager.get(tile.getImage()));
			//	tile.getX() * TILE_SIZE, tile.getY() * TILE_SIZE, TILE_SIZE, TILE_SIZE);
			spr.setBounds(TILE_OFFSET_X + tile.getX() * TILE_SIZE, TILE_OFFSET_Y + tile.getY() * TILE_SIZE, TILE_SIZE, TILE_SIZE);
			layer1.add(spr);
		}
		
		layer3 = new ArrayList<Sprite>();
		for(UTMFTile tile : map.getTileList(3))
		{
			Sprite spr = new Sprite(TextureManager.get(tile.getImage()));
			//	tile.getX() * TILE_SIZE, tile.getY() * TILE_SIZE, TILE_SIZE, TILE_SIZE);
			spr.setBounds(TILE_OFFSET_X + tile.getX() * TILE_SIZE, TILE_OFFSET_Y + tile.getY() * TILE_SIZE, TILE_SIZE, TILE_SIZE);
			layer3.add(spr);
		}
	}

	private void loadMapObjects(UTMFMap map) {
		//load map thinkers
		thinkers = new ArrayList<Thinker>();
		for(UTMFObject obj : map.getObjectList())
		{
			try
			{
				Thinker t = ThinkerManager.makeThinker(obj.getThinker(), obj.getX(), obj.getY(), obj.getFacing(), obj.getLayer(), obj.getId(), obj.getParams());
				thinkers.add(t);
			}
			catch(ThinkerNotFoundException e)
			{
				//e.printStackTrace(System.err); //TODO: do something not retarded with this
				System.err.println("Could not create" + obj.getThinker());
			} 
		}
	}

	@Override
	public void enter()
	{
		batch = new SpriteBatch();
		MusicManager.play(music, true);
	}
	
	@Override
	public void tick() {
		
		//cleanup old crap
		Iterator<Thinker> tIt = thinkers.iterator();
		while(tIt.hasNext())
		{
			if(!tIt.next().isEnabled())
				tIt.remove();
		}
		
		//hit everything
		this.hit();
		
		//run ticks
		this.run();
		
		//OVERLAY: update overlay
		SceneManager.OVERLAY_DEFAULT_HUD.update();
		
		//this.draw();

	}
	
	private void run() {
		//run phase
		
		//tick all (tickable) thinkers
		for(Thinker t : thinkers)
		{
			if(t instanceof TTickable && t.isEnabled())
				((TTickable)t).tick(this);
		}
	
	}
	
	private void hit()
	{
		
		//check blockmap collisions
		for(Thinker t : thinkers)
		{
			if(t instanceof THittable && t.isSolid())
			{
				if(t.getX() < 0 || t.getY() < 0 || t.getX() > blockmap[0].length - 1 || t.getY() > blockmap.length - 1 || blockmap[t.getY()][t.getX()])
				{
					((THittable) t).hit(this, null);
				}
			}
		}
		
		//check all thinker collisions
		//this is horrible
		for(Thinker t1 : thinkers)
		{
			if(t1 instanceof THittable && t1.isSolid())
			{
				for(Thinker t2 : thinkers)
				{
					if(t2 instanceof THittable && t2.isSolid())
					{
						if(t1 != t2)
						{
							if(t1.getX() == t2.getX() && t1.getY() == t2.getY())
							{
								((THittable) t1).hit(this, t2);
							}
						}
					}
				}
			}
		}
		
	}
	
	public void draw() {
		
		//clear the canvas
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		//begin draw
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		
		batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		batch.flush();
		
		//draw tile layer 0
		for(Sprite s : layer0)
		{
			s.draw(batch);
		}
		
		batch.flush();
		
		//draw tile layer 1
		for(Sprite s : layer1)
		{
			s.draw(batch);
		}
		
		batch.flush();
		
		//draw all thinkers
		for(Thinker t : thinkers)
		{
			if(t instanceof TDrawable && t.isVisible())
				((TDrawable)t).draw(this);
		}
		
		batch.flush();
		
		//draw tile layer 3
		for(Sprite s : layer3)
		{
			s.draw(batch);
		}
		
		//this is test code, ignore it

		
		//batch.begin();
		//batch.draw(img, 0, 0);
		//batch.draw(TextureManager.get("badlogic"), 0, 0);
		/*
		batch.draw(TextureManager.get("books"), 80, 40, 32, 32);
		batch.draw(TextureManager.get("books"), 160, 120);
		batch.draw(TextureManager.get("nope"), 400, 400);
		batch.draw(TextureManager.get("nope"), 0, 0, 80, 40);
		batch.draw(TextureManager.get("books"), 320, 240, 128, 128);
		*/
		
		batch.end();
		
		//draw overlay
		SceneManager.OVERLAY_DEFAULT_HUD.draw();
		
	}
	
	@Override
	public void exit()
	{
		batch.dispose();
		batch = null;
		//MusicManager.stop();
	}
	
	/**
	 * @param gridX The x position on the grid.
	 * @param gridY The y position on the grid.
	 * @param sprite The sprite to draw.
	 */
	public void drawAt(int gridX, int gridY, Sprite sprite)
	{
		sprite.setSize(TILE_SIZE, TILE_SIZE);
		sprite.setX(gridX * TILE_SIZE + TILE_OFFSET_X);
		sprite.setY(gridY * TILE_SIZE + TILE_OFFSET_Y);
		sprite.draw(batch);
	}
	
	/**
	 * @param gridX The x position on the grid.
	 * @param gridY The y position on the grid.
	 * @param tex The TextureRegion to draw.
	 */
	public void drawAt(int gridX, int gridY, TextureRegion tex)
	{
		batch.draw(tex, gridX * TILE_SIZE + TILE_OFFSET_X, gridY * TILE_SIZE + TILE_OFFSET_Y, TILE_SIZE, TILE_SIZE);
	}
	
	/**
	 * @param gridX The x position on the grid.
	 * @param gridY The y position on the grid.
	 * @param tex The Texture to draw.
	 */
	public void drawAt(int gridX, int gridY, Texture tex)
	{
		batch.draw(tex, gridX * TILE_SIZE + TILE_OFFSET_X, gridY * TILE_SIZE + TILE_OFFSET_Y, TILE_SIZE, TILE_SIZE);
	}
	
	/**
	 * @param gridX The x position on the grid.
	 * @param gridY The y position on the grid.
	 * @param tex The texture (from TextureManager) to draw.
	 */
	public void drawAt(int gridX, int gridY, String tex)
	{
		batch.draw(TextureManager.get(tex), gridX * TILE_SIZE + TILE_OFFSET_X, gridY * TILE_SIZE + TILE_OFFSET_Y, TILE_SIZE, TILE_SIZE);
	}
	
	/**
	 * @param gridX The x position on the grid.
	 * @param gridY The y position on the grid.
	 * @param offsetX The x offset as a fraction of a tile.
	 * @param offsetY The y offset as a fraction of a tile.
	 * @param sprite The sprite to draw.
	 */
	public void drawBetween(int gridX, int gridY, float offsetX, float offsetY, Sprite sprite)
	{
		//System.err.println(offsetX + " " + offsetY);
		sprite.setSize(TILE_SIZE, TILE_SIZE);
		sprite.setX(gridX * TILE_SIZE + TILE_OFFSET_X + TILE_SIZE * offsetX);
		sprite.setY(gridY * TILE_SIZE + TILE_OFFSET_Y + TILE_SIZE * offsetY);
		sprite.draw(batch);
	}
	
	public void setPlayerPosition(int gridX, int gridY)
	{
		player.setX(gridX);
		player.setY(gridY);
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public TextureRegion getBackground() {
		return background;
	}

	public List<Sprite> getLayer0() {
		return layer0;
	}

	public List<Sprite> getLayer1() {
		return layer1;
	}

	public List<Sprite> getLayer3() {
		return layer3;
	}

	public List<Thinker> getThinkers() {
		return thinkers;
	}

	public boolean[][] getBlockmap() {
		return blockmap;
	}
	
	


}
