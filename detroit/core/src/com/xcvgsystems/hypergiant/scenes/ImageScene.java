package com.xcvgsystems.hypergiant.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.xcvgsystems.hypergiant.managers.InputManager;
import com.xcvgsystems.hypergiant.managers.MusicManager;
import com.xcvgsystems.hypergiant.managers.SceneManager;
import com.xcvgsystems.hypergiant.managers.TextureManager;

public class ImageScene extends StaticScene {

	protected TextureRegion background;
	protected String music;
	protected String nextScene;
	
	public ImageScene(String image, String music, String nextScene)
	{
		camera= new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

		if(image != null)
			background = TextureManager.get(image);
		
		this.music = music;
		this.nextScene = nextScene;
		
	}
	
	@Override
	public void enter()
	{
		batch = new SpriteBatch();
		MusicManager.play(music, true);
	}

	@Override
	public void tick() {
		//check for input
		if(InputManager.isUsePressed())
			SceneManager.changeScene(nextScene);
	}

	@Override
	public void draw() {
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.end();
	}

	@Override
	public void exit()
	{
		batch = null;
		MusicManager.stop();
	}

}
