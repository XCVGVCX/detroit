package com.xcvgsystems.hypergiant.thinkers;

import com.xcvgsystems.hypergiant.scenes.WorldScene;

public interface TUseable {
	
	public void use(WorldScene context, Thinker other);

}
