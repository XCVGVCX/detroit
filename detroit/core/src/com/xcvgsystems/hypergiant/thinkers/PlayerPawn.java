package com.xcvgsystems.hypergiant.thinkers;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.xcvgsystems.hypergiant.managers.InputManager;
import com.xcvgsystems.hypergiant.managers.TextureManager;
import com.xcvgsystems.hypergiant.scenes.WorldScene;
import com.xcvgsystems.hypergiant.utils.SpriteHelper;

/**
 * PlayerPawn: A special Thinker that represents the player.
 * 
 * @author Chris
 *
 */
public class PlayerPawn extends Thinker implements TDrawable, THittable, TTickable {
	
	final String GRAPHIC_NAME = "Player";
	
	final float SPEED = 0.1f;
	final float SPEED_RUN = 0.2f;
	
	//technically, all these frames are ticks... sorry lol
	final int IDLE_FRAMES = 15;
	final int WALK_FRAMES = 4; 
	final int WALK_FRAMES_RUN = 2;
	
	protected ActorState state;
	
	protected int oldX, oldY;
	protected int animationFrame; //frame count for any animation
	protected float animationPosX; //position of walking animation
	protected float animationPosY; //position of walking animation
	protected boolean running; //am I running?
	protected int stateFrames; //frames for this state; meant for walking
	
	protected Sprite[][] sprites = new Sprite[8][8]; //rows: ABCDEFGH cols: 1357
	
	//temporary
	protected Sprite currentSprite;
	
	public PlayerPawn(int x, int y)
	{
		this.x = x;
		this.y = y;
		this.facing = 1;
		
		//load all Sprites TODO: figure out a nicer way to do this. and maybe not store sprites here
		/*
		sprites[0][1] = new Sprite(TextureManager.get("PlayerA1"));
		sprites[0][3] = new Sprite(TextureManager.get("PlayerA3"));
		sprites[0][5] = new Sprite(TextureManager.get("PlayerA5"));
		sprites[0][7] = new Sprite(TextureManager.get("PlayerA7"));
		sprites[1][1] = new Sprite(TextureManager.get("PlayerB1"));
		sprites[1][3] = new Sprite(TextureManager.get("PlayerB3"));
		sprites[1][5] = new Sprite(TextureManager.get("PlayerB5"));
		sprites[1][7] = new Sprite(TextureManager.get("PlayerB7"));
		sprites[2][1] = new Sprite(TextureManager.get("PlayerC1"));
		sprites[2][3] = new Sprite(TextureManager.get("PlayerC3"));
		sprites[2][5] = new Sprite(TextureManager.get("PlayerC5"));
		sprites[2][7] = new Sprite(TextureManager.get("PlayerC7"));
		sprites[3][1] = new Sprite(TextureManager.get("PlayerD1"));
		sprites[3][3] = new Sprite(TextureManager.get("PlayerD3"));
		sprites[3][5] = new Sprite(TextureManager.get("PlayerD5"));
		sprites[3][7] = new Sprite(TextureManager.get("PlayerD7"));
		sprites[4][1] = new Sprite(TextureManager.get("PlayerE1"));
		sprites[4][3] = new Sprite(TextureManager.get("PlayerE3"));
		sprites[4][5] = new Sprite(TextureManager.get("PlayerE5"));
		sprites[4][7] = new Sprite(TextureManager.get("PlayerE7"));
		*/
		//TODO: finish
		
		SpriteHelper.getSprites(GRAPHIC_NAME, sprites);
		
		currentSprite = sprites[0][1];
		
		this.state = ActorState.IDLING;
	}

	@Override
	public void tick(WorldScene context) {
		//I am a player, I do nothing here
		//or should I put player movement handling here?
		
		//do different things based on state
		switch(state)
		{
		case DEAD:
			//dead: player is dead, do nothing
			
			break;
		case DORMANT:
			//dormant: do, uh, nothing?
			
			break;
		case DYING:
			//dying: handle dying animation
			
			break;
		case IDLING:
			//idling: check player input and do stuff, handle idle animation
			
			if(InputManager.isDirectionPressed())
			{
				//we want to move somewhere
				
				int newFacing = InputManager.inputDirectionToFacing(InputManager.getDirectionPressed());
				
				if(!(newFacing == this.facing)) //we want to change facing
				{
					this.facing = newFacing;
				}
				else //we want to move
				{
					running = InputManager.isShiftPressed();
					startMoving();
				}
				
				System.err.println(InputManager.getDirectionPressed());
			}
			else
			{
				//animate idling
				animateIdling();
			}
			
			break;
		case MOVING:
			//moving: handle moving animation
			
			//handle the actual animation of frames in the draw
			//handle only the moving of the object here
			

			//move the thing
			animationPosX += velocityX;
			animationPosY += velocityY;
			if(Math.abs(animationPosX + animationPosY) >= 0.999f)
			{
				finishMoving();
			}
			else
			{
				animateMoving();
			}
			
			break;
		case SPAWNING:
			//spawning: handle spawn animation
			
			break;
		default:
			break;
		
		}
		
		return;
	}

	private void animateMoving() {
		//play animation
		animationFrame++;
		if(animationFrame < stateFrames)
		{
			currentSprite = sprites[3][facing];
		}
		else if(animationFrame < stateFrames * 2)
		{
			currentSprite = sprites[4][facing];
		}
		else
		{
			animationFrame = 0;
		}
	}

	private void startMoving()
	{
		float speed;
		if(running)
		{
			speed = SPEED_RUN;
			stateFrames = WALK_FRAMES;
		}
		else
		{
			speed = SPEED;
			stateFrames = WALK_FRAMES_RUN;
		}
		
		oldX = x;
		oldY = y;
		
		if(facing == 1)
		{
			velocityY += speed;
			this.y += 1;
		}
		else if(facing == 3)
		{
			velocityX -= speed;
			this.x -= 1;
		}
		else if(facing == 5)
		{
			velocityY -= speed;
			this.y -= 1;
		}
		else if(facing == 7)
		{
			velocityX += speed;
			this.x += 1;
		}
		//animationFrame = 0;
		animationPosX = 0f;
		animationPosY = 0f;
		
		this.state = ActorState.MOVING;
	}
	
	private void finishMoving()
	{
		
		/*
		if(facing == 1)
		{
			this.y += 1;
		}
		else if(facing == 3)
		{
			this.x -= 1;
		}
		else if(facing == 5)
		{
			this.y -= 1;
		}
		else if(facing == 7)
		{
			this.x += 1;
		}
		*/
		
		velocityX = 0f;
		velocityY = 0f;
		//animationFrame = 0;
		animationPosX = 0f;
		animationPosY = 0f;
		
		this.state = ActorState.IDLING;
	}

	private void animateIdling() {
		//play animation
		animationFrame++;
		if(animationFrame < IDLE_FRAMES)
		{
			currentSprite = sprites[1][facing];
		}
		else if(animationFrame < IDLE_FRAMES * 2)
		{
			currentSprite = sprites[2][facing];
		}
		else
		{
			animationFrame = 0;
		}
		
	}

	@Override
	public void draw(WorldScene context) {

		
		if(state != ActorState.MOVING)
		{
			//draw me
			context.drawAt(this.x, this.y, currentSprite);
		}
		else
		{
			//moving actors are a speshful case
			context.drawBetween(this.oldX, this.oldY, animationPosX, animationPosY, currentSprite);
		}
		


	}

	@Override
	public void hit(WorldScene context, Thinker other) {
		
		System.err.println("HIT!");
		
		if(other == null)
		{
			//if other is null, then we've hit the BLOCKMAP
			this.x = oldX;
			this.y = oldY;
			finishMoving();
			return;
		}
		
		//I am the player, I will rely on the other Thinker to do something
		//although I will arrest my motion(?)
		
		return;
	}

}
