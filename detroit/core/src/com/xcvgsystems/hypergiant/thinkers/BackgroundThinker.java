package com.xcvgsystems.hypergiant.thinkers;

import com.xcvgsystems.hypergiant.scenes.WorldScene;

/**
 * BackgroundThinker: An invisible, non-solid thinker.
 * 
 * @author Chris
 *
 */
public abstract class BackgroundThinker extends Thinker implements TTickable {

	@Override
	public boolean isVisible() {
		return false;
	}

	@Override
	public void setVisible(boolean visible) {
		if(visible)
			throw new IllegalArgumentException("BackgroundThinker cannot be visible");
	}

	@Override
	public boolean isSolid() {
		return false;
	}

	@Override
	public void setSolid(boolean solid) {
		if(solid)
			throw new IllegalArgumentException("BackgroundThinker cannot be solid");
	}
	
	

}
