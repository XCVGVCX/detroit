package com.xcvgsystems.hypergiant.thinkers;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.xcvgsystems.hypergiant.managers.TextureManager;
import com.xcvgsystems.hypergiant.scenes.WorldScene;

public class ItemGiver extends Thinker implements TDrawable, THittable, TUseable {

	protected String item;
	protected Sprite DEFAULT_SPRITE = new Sprite(TextureManager.get("!THINKER"));
	protected Sprite currentSprite;
	
	//TODO: comments, fadeout
	
	public ItemGiver()
	{
		currentSprite = DEFAULT_SPRITE;
	}
	
	public ItemGiver(int x, int y, int facing, int id, String item)
	{
		this();
		this.x = x;
		this.y = y;
		this.facing = facing;
		this.id = id;
		this.item = item;
	}
	
	@Override
	public void use(WorldScene context, Thinker other) {
		if(other instanceof PlayerPawn)
		{
			this.enabled = false;
			giveItem();
		}
		
	}

	@Override
	public void hit(WorldScene context, Thinker other) {
		if(other instanceof PlayerPawn)
		{
			this.enabled = false;
			giveItem();
		}
	}
	
	//TODO: actual item giving
	private void giveItem()
	{
		System.out.println("PICKUP" + item);
	}

	@Override
	public void draw(WorldScene context) {
		// TODO Auto-generated method stub
		context.drawAt(this.x, this.y, this.currentSprite);
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}
	
	

}
