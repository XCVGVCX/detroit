package com.xcvgsystems.hypergiant.thinkers;

import com.xcvgsystems.hypergiant.scenes.WorldScene;

public abstract class EffectThinker extends Thinker implements TDrawable {

	@Override
	public boolean isSolid() {
		return false;
	}

	@Override
	public void setSolid(boolean solid) {
		throw new IllegalArgumentException("EffectThinker cannot be solid");
	}

}
