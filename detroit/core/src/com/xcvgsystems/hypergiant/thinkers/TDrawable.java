package com.xcvgsystems.hypergiant.thinkers;

import com.xcvgsystems.hypergiant.scenes.WorldScene;

public interface TDrawable {

	public void draw(WorldScene context);
	
}
