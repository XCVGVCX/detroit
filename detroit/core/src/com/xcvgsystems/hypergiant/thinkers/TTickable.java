package com.xcvgsystems.hypergiant.thinkers;

import com.xcvgsystems.hypergiant.scenes.WorldScene;

public interface TTickable {
	
	public void tick(WorldScene context);

}
