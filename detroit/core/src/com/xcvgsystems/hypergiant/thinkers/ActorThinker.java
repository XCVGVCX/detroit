package com.xcvgsystems.hypergiant.thinkers;

import com.xcvgsystems.hypergiant.scenes.WorldScene;

public abstract class ActorThinker extends Thinker implements TDrawable, THittable, TTickable, TUseable {

	protected ActorState state;
	protected boolean active;

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}
