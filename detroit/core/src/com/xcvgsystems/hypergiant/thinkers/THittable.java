package com.xcvgsystems.hypergiant.thinkers;

import com.xcvgsystems.hypergiant.scenes.WorldScene;

public interface THittable {
	
	public void hit(WorldScene context, Thinker other);

}
