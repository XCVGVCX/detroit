package com.xcvgsystems.hypergiant.thinkers;

/**
 * MapSpot: A minimal Thinker that stores a point on the map, meant to be used by other things.
 * @author Chris
 *
 */
public class MapSpot extends Thinker {
	
	public MapSpot()
	{
		super(); //this isn't necessary I don't think but it makes me feel better
	}
	
	public MapSpot(int x, int y, int facing, int id)
	{
		this.x = x;
		this.y = y;
		this.facing = facing;
		this.id = id;
	}	
}
