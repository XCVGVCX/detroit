package com.xcvgsystems.hypergiant.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

//not sure how libGDX deals with input, so... abstract it

/**
 * Provides input services.
 * @author Chris
 *
 */
public class InputManager {

	/**
	 * Represents an input direction.
	 * @author Chris
	 *
	 */
	public enum InputDirection
	{

		UP,
		DOWN, 
		LEFT,
		RIGHT
	}
	
	private static InputDirection inputDirection;
	
	/**
	 * Initialize the InputManager
	 */
	public static void init()
	{
		System.out.print("InputManager.init...");
		
		System.out.println("done!");
	}
	
	/**
	 * Poll the input
	 */
	public static void poll()
	{
		getDirectionKeys();
	}
	
	/**
	 * Dispose of the InputManager
	 */
	public static void dispose()
	{
		System.out.print("InputManager.dispose...");
		
		System.out.println("done!");
	}
	
	/**
	 * Check if some direction is pressed.
	 * @return if a direction is pressed
	 */
	public static boolean isDirectionPressed()
	{

		if(inputDirection != null)
			return true;
		
		return false;
	}
	
	/**
	 * Get the direction that is pressed.
	 * @return the direction that is pressed
	 */
	public static InputDirection getDirectionPressed()
	{
		return inputDirection;
		
	}
	
	/**
	 * Get the direction that is pressed as facing value.
	 * @return the direction that is pressed (as facing value)
	 */
	public static int getFacingPressed()
	{
		return inputDirectionToFacing(inputDirection);
	}
	
	/**
	 * Check if the use key is pressed.
	 * @return if the use key is pressed
	 */
	public static boolean isUsePressed()
	{
		return Gdx.input.isKeyPressed(Input.Keys.SPACE);
		
	}
	
	/**
	 * Check if the escape key is pressed.
	 * @return if the escape key is pressed
	 */
	public static boolean isEscPressed()
	{
		return Gdx.input.isKeyPressed(Input.Keys.ESCAPE);	
	}
	
	/**
	 * Check if the shift key is pressed.
	 * @return if the shift key is pressed
	 */
	public static boolean isShiftPressed()
	{
		return Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT);
	}
	
	/**
	 * Converts an InputDirection enum to a facing value.
	 * @param direction the InputDirection to convert
	 * @return the facing value
	 */
	public static int inputDirectionToFacing(InputDirection direction)
	{
		if(direction == InputDirection.UP)
		{
			return 5;
		}
		else if(direction == InputDirection.DOWN)
		{
			return 1;
		}
		else if(direction == InputDirection.RIGHT)
		{
			return 7;
		}
		else if(direction == InputDirection.LEFT)
		{
			return 3;
		}
		
		return 0;
	}
	
	/**
	 * Converts a facing value to an InputDirection enum.
	 * @param facing the facing value to convert
	 * @return the InputDirection
	 */
	public static InputDirection facingToInputDirection(int facing)
	{
		
		if(facing == 1)
		{
			return InputDirection.DOWN;
		}
		else if(facing == 3)
		{
			return InputDirection.LEFT;
		}
		else if(facing == 5)
		{
			return InputDirection.UP;
		}
		else if(facing == 7)
		{
			return InputDirection.RIGHT;
		}
		
		return null;
	}

	private static void getDirectionKeys()
	{
		
		//are NEW keys pressed?
		if(Gdx.input.isKeyJustPressed(Input.Keys.UP))
		{
			inputDirection = InputDirection.UP;
			return;
		}
		else if(Gdx.input.isKeyJustPressed(Input.Keys.DOWN))
		{
			inputDirection = InputDirection.DOWN;
			return;
		}
		else if(Gdx.input.isKeyJustPressed(Input.Keys.LEFT))
		{
			inputDirection = InputDirection.LEFT;
			return;
		}
		else if(Gdx.input.isKeyJustPressed(Input.Keys.RIGHT))
		{
			inputDirection = InputDirection.RIGHT;
			return;
		}
		
		//if no direction keys, null it
		if(!(
				Gdx.input.isKeyPressed(Input.Keys.UP) ||
				Gdx.input.isKeyPressed(Input.Keys.DOWN) ||
				Gdx.input.isKeyPressed(Input.Keys.LEFT) ||
				Gdx.input.isKeyPressed(Input.Keys.RIGHT)
				))
		{
			inputDirection = null;
		}
		
	}
	

	
}
