package com.xcvgsystems.hypergiant.managers;

import com.xcvgsystems.hypergiant.exceptions.ThinkerNotFoundException;
import com.xcvgsystems.hypergiant.thinkers.*;

/**
 * Provides Thinker services.
 * @author Chris
 *
 */
public class ThinkerManager {

	private final static int DEFAULT_LAYER = 2;
	private final static int DEFAULT_FACING = 0;
	
	/**
	 * Initialize the ThinkerManager.
	 */
	public static void init()
	{
		System.out.print("ThinkerManager.init...");
		
		System.out.println("done!");
	}
	
	/**
	 * Dispose of the ThinkerManager.
	 */
	public static void dispose()
	{
		System.out.print("ThinkerManager.dispose...");
		
		System.out.println("done!");
	}

	/**
	 * Create a new Thinker
	 * @param tname the thinker to create
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param facing the direction to face
	 * @param layer the layer to be in
	 * @param id the Thinker id
	 * @param params any parameters
	 * @return the new Thinker
	 * @throws ThinkerNotFoundException if no matching Thinker was found
	 */
	public static Thinker makeThinker(String tname, int x, int y, int facing, int layer, int id, String... params) throws ThinkerNotFoundException
	{
		
		if(params == null || params.length <= 0)
		{
			params = new String[] {null,null,null,null,null};
		}
		
		//else ifs because switching on strings didn't exist yet
		if(tname.equalsIgnoreCase("MapSpot"))
		{
			return new MapSpot(x, y, facing, id);
		}
		else if(tname.equalsIgnoreCase("ItemGiver"))
		{
			return new ItemGiver(x, y, facing, id, params[0]);
		}
		else
		{
			throw new ThinkerNotFoundException();
		}
	}
	
	/**
	 * Create a new Thinker.
	 * @param tname the thinker to create
	 * @return the new Thinker
	 * @throws ThinkerNotFoundException if no matching Thinker was found
	 */
	public static Thinker makeThinker(String tname) throws ThinkerNotFoundException
	{
		return makeThinker(tname, 0, 0, DEFAULT_LAYER, DEFAULT_FACING, 0);
	}
	
}
