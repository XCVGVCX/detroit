package com.xcvgsystems.hypergiant.managers;

import java.util.*;

import com.xcvgsystems.hypergiant.models.*;

/**
 * Provides model services.
 * @author Chris
 *
 */
public class ModelManager {
	

	private static Map<String, ItemModel> baseItemModels;
	private static Map<String, MonsterModel> baseMonsterModels;

	/**
	 * Initialize the ModelManager
	 */
	public static void init()
	{
		System.out.print("ModelManager.init...");
		
		baseItemModels = new HashMap<String, ItemModel>();
		baseMonsterModels = new HashMap<String, MonsterModel>();
		
		//TODO: load models
		
		System.out.println("done!");
	}

	/**
	 * Dispose of the ModelManager
	 */
	public static void dispose()
	{
		System.out.print("ModelManager.dispose...");
		
		
		baseItemModels = null;
		baseMonsterModels = null;
		
		System.out.println("done!");
	}


	
	
	
}
