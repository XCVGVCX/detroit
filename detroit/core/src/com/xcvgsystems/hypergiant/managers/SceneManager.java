package com.xcvgsystems.hypergiant.managers;

import java.util.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.xcvgsystems.hypergiant.GVars;
import com.xcvgsystems.hypergiant.exceptions.SceneNotFoundException;
import com.xcvgsystems.hypergiant.overlays.Overlay;
import com.xcvgsystems.hypergiant.overlays.WorldHUDOverlay;
import com.xcvgsystems.hypergiant.scenes.*;
import com.xcvgsystems.hypergiant.utmf.UTMFLoader;
import com.xcvgsystems.hypergiant.utmf.UTMFMap;

/**
 * Provides scene services.
 * @author Chris
 *
 */
public class SceneManager {
	
	//eventually the overlay system will be much more advanced
	public static Overlay OVERLAY_DEFAULT_HUD;
	
	static Map<String,Scene> scenes;

	static Scene currentScene;

	private static boolean running;
	private static boolean drawing;
	private static long tickNum;
	
	/**
	 * Initialize the SceneManager, loading all maps as scenes.
	 */
	public static void init()
	{
		System.out.print("SceneManager.init...");
		
		scenes = new HashMap<String, Scene>();
		tickNum = 0;
		
		//load all maps (might move this)
		FileHandle gamePath = Gdx.files.internal(GVars.GAME_PATH);
		FileHandle mapsPath = gamePath.child("maps");
		loadAllMaps(mapsPath);
		
		OVERLAY_DEFAULT_HUD = new WorldHUDOverlay();
		
		System.out.println("done!");
	}
	
	/**
	 * Advance one tick on the current scene.
	 */
	public static void tick()
	{
		if(running)
		{
			currentScene.tick();
			tickNum++;
		}
	}
	
	/**
	 * Draw the current scene.
	 */
	public static void draw()
	{
		if(drawing)
			currentScene.draw();
	}
		
	/**
	 * Dispose of the SceneManager.
	 */
	public static void dispose()
	{
		System.out.print("SceneManager.dispose...");
		
		scenes = null;
		currentScene = null;
		
		System.out.println("done!");
	}

	/**
	 * Change scenes.
	 * @param scene the new scene
	 */
	public static void changeScene(String scene)
	{
		
		scene = scene.toUpperCase(Locale.ROOT);

		
		if(scenes.containsKey(scene))
		{	
			if(currentScene != null)
				currentScene.exit();
			currentScene = scenes.get(scene);
			currentScene.enter();
		}	
		
	}
	
	/**
	 * Change scenes to a new WorldScene, specify player position.
	 * @param scene the new scene
	 * @param playerX player start X
	 * @param playerY player stary Y
	 */
	public static void changeWorldScene(String scene, int playerX, int playerY)
	{
		scene = scene.toUpperCase(Locale.ROOT);
		
		if(scenes.containsKey(scene))
		{		
			if(currentScene != null)
				currentScene.exit();
			currentScene = scenes.get(scene);
			((WorldScene)currentScene).setPlayerPosition(playerX, playerY);
			
			currentScene.enter();
		}
	}
	
	/**
	 * Load a map file as a scene.
	 * @param file the map file to load
	 */
	public static void loadMap(FileHandle file)
	{
		try {
			UTMFMap myMap = UTMFLoader.loadMap(file.readString());
			scenes.put(file.nameWithoutExtension().toUpperCase(Locale.ROOT), new WorldScene(myMap));
		} catch (Exception e) {
			System.err.println("Could not load " + file.toString());
		}
	}
	
	/**
	 * Loads all map files in a directory
	 * @param dir The directory to look in.
	 */
	public static void loadAllMaps(FileHandle dir)
	{
		FileHandle[] files = dir.list();
		
		for(FileHandle file : files)
		{
			loadMap(file);	
		}
	}
	
	/**
	 * Adds a scene to the scene manager.
	 * @param name the name of the scene to add
	 * @param scene the scene to add
	 */
	public static void addScene(String name, Scene scene)
	{
		name = name.toUpperCase(Locale.ROOT);
		scenes.put(name, scene);
	}
	
	/**
	 * Drops a scene from the scene manager.
	 * @param name the scene to remove
	 */
	public static void dropScene(String name)
	{
		name = name.toUpperCase(Locale.ROOT);
		scenes.remove(name);
	}

	/**
	 * @return if the scene is running
	 */
	public static boolean isRunning() {
		return running;
	}

	/**
	 * @param running whether the scene is running
	 */
	public static void setRunning(boolean running) {
		SceneManager.running = running;
	}

	/**
	 * @return if the scene is drawing
	 */
	public static boolean isDrawing() {
		return drawing;
	}

	/**
	 * @param drawing whether the scene is drawing
	 */
	public static void setDrawing(boolean drawing) {
		SceneManager.drawing = drawing;
	}
	
	/**
	 * @return the current number of ticks elapsed
	 */
	public static long getTicks() {
		return tickNum;
	}

	/**
	 * @return the current Scene
	 */
	public static Scene getCurrentScene() {
		return currentScene;
	}	
	
	
}
