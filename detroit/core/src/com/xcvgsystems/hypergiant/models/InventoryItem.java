package com.xcvgsystems.hypergiant.models;

/**
 * Represents an item in an inventory.
 * 
 * @author Chris
 *
 */
public final class InventoryItem
{

	private final ItemModel item;
	private int value; //can represent damage or another item-specific value
	
	/**
	 * Creates a new InventoryItem representing the specified ItemModel.
	 * @param item the ItemModel
	 */
	public InventoryItem(final ItemModel item)
	{
		this.item = item;
	}
	

	/**
	 * Creates a new InventoryItem representing the specified ItemModel.
	 * @param item the ItemModel
	 * @param value the damage value
	 */
	public InventoryItem(final ItemModel item, int value)
	{
		this.item = item;
		this.value = value;
	}

	/**
	 * @return the damage value of this item
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the damage value of this item
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * @return the underlying item model
	 */
	public ItemModel getItem() {
		return item;
	}

}
