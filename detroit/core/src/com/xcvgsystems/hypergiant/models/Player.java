package com.xcvgsystems.hypergiant.models;

import java.util.List;

/**
 * A model that represents a player.
 * @author Chris
 *
 */
public class Player
{
	
	public enum Gender { UNSPECIFIED, FEMALE, MALE }
	
	private int hp; //health points
	private int sp; //shield/armor/barrier points
	private int mp; //mana/energy points
	private Gender sex; //what am I?
	
	private List<InventoryItem> inventory;

}
