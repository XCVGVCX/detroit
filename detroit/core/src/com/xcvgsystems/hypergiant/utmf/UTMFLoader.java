package com.xcvgsystems.hypergiant.utmf;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UTMFLoader {
	
	//TODO: resilience
	//TODO: UTMF 1.1
	
	//amazingly, this piece of junk actually works
	
	public static UTMFMap loadMap(String strMap)
	{
		UTMFMap map;
		
		Scanner scan = new Scanner(strMap);
		
		int width = 0, height = 0, startx = 0, starty = 0;
		String background = "!NULL";
		String name = "";
		String music = "!NULL";
		
		List<String> tiles = new ArrayList<String>();
		List<String> objects = new ArrayList<String>();
		
		while(scan.hasNext())
		{
			String str = scan.nextLine();
			
			if(str.startsWith("tile"))
			{
				//deal with a tile
				String strTile = "";
				String subStr;
				scan.nextLine();
				do
				{
					subStr = scan.nextLine();
					strTile += subStr  + "\n";
				} while(!subStr.contains("}"));
				
				tiles.add(strTile);
			}
			else if(str.startsWith("object"))
			{
				//deal with an object
				String strObject = "";
				String subStr;
				scan.nextLine();
				do
				{
					subStr = scan.nextLine();
					strObject += subStr + "\n";
				} while(!subStr.contains("}"));
				
				objects.add(strObject);
			}
			else if(str.startsWith("width"))
			{
				width = Integer.parseInt(str.trim().substring(6, str.length()-1));
				//System.out.println(width);
			}
			else if(str.startsWith("startx"))
			{
				startx = Integer.parseInt(str.trim().substring(7, str.length()-1));
			}
			else if(str.startsWith("starty"))
			{
				starty = Integer.parseInt(str.trim().substring(7, str.length()-1));
			}
			else if(str.startsWith("height"))
			{
				height = Integer.parseInt(str.trim().substring(7, str.length()-1));
				//System.out.println(height);
			}
			else if(str.startsWith("background"))
			{
				background = str.trim().substring(12, str.length()-2);
				//System.out.println(background);
			}
			else if(str.startsWith("music"))
			{
				music = str.trim().substring(7, str.length()-2);
				//System.out.println(music);
			}
			else if(str.startsWith("name"))
			{
				name = str.trim().substring(6, str.length()-2);
				//System.out.println(name);
			}
		}
		
		map = new UTMFMap(width,height);
		map.background = background;
		map.startX = startx;
		map.startY = starty;
		map.music = music;
		map.name = name;
		
		for(String strTile : tiles)
		{
			UTMFTile myTile = parseTile(strTile);
			map.getTileLayer(myTile.layer)[myTile.y][myTile.x] = myTile;
		}
		
		for(String strObj : objects)
		{
			UTMFObject myObj = parseObject(strObj);
			map.getObjects().add(myObj);
		}
		
		return map;
	}
	
	public static UTMFTile parseTile(String strTile)
	{
		Scanner scan = new Scanner(strTile);
		int x = 0;
		int y = 0;
		String image = null;
		int layer = 0;
		boolean blocking = false;
		
		while(scan.hasNext())
		{
			String line = scan.nextLine().trim();
			if(line.startsWith("x"))
			{
				x = Integer.parseInt(line.substring(2, line.length() - 1));
			}
			else if(line.startsWith("y"))
			{
				y = Integer.parseInt(line.substring(2, line.length() - 1));
			}
			else if(line.startsWith("image"))
			{
				image = line.substring(7, line.length() - 2);
			}
			else if(line.startsWith("layer"))
			{
				layer = Integer.parseInt(line.substring(6, line.length() - 1));
			}
			else if(line.startsWith("blocking"))
			{
				blocking = Boolean.parseBoolean(line.substring(9, line.length() - 1));
			}
		}
		
		
		scan.close();
		return new UTMFTile(x, y, image, layer, blocking);
	}
	
	public static UTMFObject parseObject(String strObject)
	{
		Scanner scan = new Scanner(strObject);
		int id = 0;
		int x = 0;
		int y = 0;
		String thinker = null;
		int layer = 2;
		int facing = 0;
		String[] params = new String[5];
		
		while(scan.hasNext())
		{
			String line = scan.nextLine().trim();
			if(line.startsWith("x"))
			{
				x = Integer.parseInt(line.substring(2, line.length() - 1));
			}
			else if(line.startsWith("y"))
			{
				y = Integer.parseInt(line.substring(2, line.length() - 1));
			}
			else if(line.startsWith("thinker"))
			{
				thinker = line.substring(9, line.length() - 2);
			}
			else if(line.startsWith("layer"))
			{
				layer = Integer.parseInt(line.substring(6, line.length() - 1));
			}
			else if(line.startsWith("facing"))
			{
				facing = Integer.parseInt(line.substring(7, line.length() - 1));
			}
			else if(line.startsWith("param"))
			{
				int idx = Integer.parseInt(line.substring(5, 6));
				String param = line.substring(7, line.length() - 1).replace("\"", "").trim();
				params[idx] = param;
			}

		}
		
		
		scan.close();
		return new UTMFObject(id, x, y, facing, thinker, layer, params);
	}

}
