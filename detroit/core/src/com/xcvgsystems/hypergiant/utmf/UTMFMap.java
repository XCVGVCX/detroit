package com.xcvgsystems.hypergiant.utmf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UTMFMap {
	
	protected int width;
	protected int height;
	protected int startX;
	protected int startY;
	protected String background;
	protected String music;
	protected String name;
	
	protected UTMFTile[][] tiles0;
	protected UTMFTile[][] tiles1;
	protected UTMFTile[][] tiles2;
	protected UTMFTile[][] tiles3;
	protected List<UTMFObject> objects;
	
	public UTMFMap(int sizeX, int sizeY)
	{
		width = sizeX;
		height = sizeY;
		
		//I might turn this sideways
		tiles0 = new UTMFTile[height][width];
		tiles1 = new UTMFTile[height][width];
		tiles2 = new UTMFTile[height][width];
		tiles3 = new UTMFTile[height][width];
		
		objects = new ArrayList<UTMFObject>();
	}
	
	//indirect access: these are intended for a game
	
	public List<UTMFTile> getTileList (int layer)
	{
		List<UTMFTile> tiles = new ArrayList<UTMFTile>();
		
		UTMFTile[][] tileLayer;
		
		switch(layer)
		{
		case 0:
			tileLayer = tiles0;
			break;
		case 1:
			tileLayer = tiles1;
			break;
		case 2:
			tileLayer = tiles2;
			break;
		case 3:
			tileLayer = tiles3;
			break;
		default:
			return tiles;
		}
		
		for(UTMFTile[] row : tileLayer)
		{
			for(UTMFTile tile : row)
				if(tile != null)
					tiles.add(tile);
		}
		
		return tiles;
	}
	
	public List<UTMFObject> getObjectList()
	{
		return new ArrayList<UTMFObject>(objects); //copying is probably safer
		
	}
	
	public boolean[][] getBlockMap()
	{
		
		boolean[][] blockmap = new boolean[height][width];
		
		for(int row = 0; row < blockmap.length; row++)
		{
			for(int col = 0; col < blockmap[0].length; col++)
			{
				blockmap[row][col] = false;
			}
		}
		
		for(int row = 0; row < tiles0.length; row++)
		{
			for(int col = 0; col < tiles0[row].length; col++)
			{
				if(tiles0[row][col] != null && tiles0[row][col].isBlocking())
					blockmap[row][col] = true;
			}
		}
		for(int row = 0; row < tiles1.length; row++)
		{
			for(int col = 0; col < tiles1[row].length; col++)
			{
				if(tiles1[row][col] != null && tiles1[row][col].isBlocking())
					blockmap[row][col] = true;
			}
		}
		for(int row = 0; row < tiles2.length; row++)
		{
			for(int col = 0; col < tiles2[row].length; col++)
			{
				if(tiles2[row][col] != null && tiles2[row][col].isBlocking())
					blockmap[row][col] = true;
			}
		}
		for(int row = 0; row < tiles3.length; row++)
		{
			for(int col = 0; col < tiles3[row].length; col++)
			{
				if(tiles3[row][col] != null && tiles3[row][col].isBlocking())
					blockmap[row][col] = true;
			}
		}
		
		return blockmap;
	}
	
	//direct access: these are intended for an editor
	
	public UTMFTile[][] getTileLayer(int lnum)
	{
		switch(lnum)
		{
		case 0:
			return tiles0;
		case 1:
			return tiles1;
		case 2:
			return tiles2;
		case 3:
			return tiles3;
		default:
			return null;
		}
	}
	
	public List<UTMFObject> getObjects()
	{
		return objects;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getStartX() {
		return startX;
	}

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public int getStartY() {
		return startY;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getMusic() {
		return music;
	}

	public void setMusic(String music) {
		this.music = music;
	}

	public void setObjects(List<UTMFObject> objects) {
		this.objects = objects;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	



}
