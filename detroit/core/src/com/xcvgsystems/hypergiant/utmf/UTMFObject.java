package com.xcvgsystems.hypergiant.utmf;

public class UTMFObject {
	
	protected int id;
	protected int x, y;
	protected int facing;
	protected String thinker; //we don't want to store the actual thinker here
	protected int layer; //doesn't actually matter yet
	protected String[] params;
	
	protected boolean enabled = true;
	protected boolean solid = true;
	protected boolean visible = true;
	
	/**
	 * @param id
	 * @param x
	 * @param y
	 * @param thinker
	 * @param layer
	 * @param params
	 */
	public UTMFObject(int id, int x, int y, int facing, String thinker, int layer,
			String[] params) {
		super();
		this.id = id;
		this.x = x;
		this.y = y;
		this.thinker = thinker;
		this.layer = layer;
		this.params = params;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getThinker() {
		return thinker;
	}

	public void setThinker(String thinker) {
		this.thinker = thinker;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public String[] getParams() {
		return params;
	}

	public void setParams(String[] params) {
		this.params = params;
	}
	
	
	public String getParam(int pnum)
	{
		return params[pnum];
	}
	
	public void setParam(String param, int pnum)
	{
		params[pnum] = param;
	}
	

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isSolid() {
		return solid;
	}

	public void setSolid(boolean solid) {
		this.solid = solid;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	@Override
	public String toString()
	{
		String output = "OBJECT(" + x + "," + y + "|" + layer + ")" + " " + thinker + ": ";
		for(String pm : params)
		{
			output += pm + ",";
		}
		return output;
				
	}

	public int getFacing() {
		return facing;
	}

	public void setFacing(int facing) {
		this.facing = facing;
	}
	
	
	
}
