package com.xcvgsystems.hypergiant.utmf;

public class UTMFTile {
	protected int x, y;
	protected String image;
	protected int layer;
	protected boolean blocking;
	
	/**
	 * @param x
	 * @param y
	 * @param image
	 * @param layer
	 * @param blocking
	 */
	public UTMFTile(int x, int y, String image, int layer, boolean blocking) {
		super();
		this.x = x;
		this.y = y;
		this.image = image;
		this.layer = layer;
		this.blocking = blocking;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public boolean isBlocking() {
		return blocking;
	}

	public void setBlocking(boolean blocking) {
		this.blocking = blocking;
	}
	
	@Override
	public String toString()
	{
		return "TILE(" + x + "," + y + "|" + layer + ")" + " " + image + " " + blocking;
	}
	
}
