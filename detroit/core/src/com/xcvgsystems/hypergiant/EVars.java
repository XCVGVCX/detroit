package com.xcvgsystems.hypergiant;

/**
 * EVars contains static and final vars used by the engine.
 * @author Chris
 *
 */
public class EVars {

	public static final String BASE_PATH = "hypergiant.xgf";
	
	public static final int WINDOW_X = 640;
	public static final int WINDOW_Y = 480;
	
	public static final boolean DEBUG = true;
	
}
