package com.xcvgsystems.detroit.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.xcvgsystems.hypergiant.*;
import com.xcvgsystems.detroit.Detroit;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = GVars.GAME_NAME;
	    config.width = EVars.WINDOW_X;
	    config.height = EVars.WINDOW_Y;
		new LwjglApplication(new Detroit(), config);
	}
}
