//comments should be escaped with double slashes

//HEADER
namespace="hypergiant"; //this is always the same. Note that comments can be on a line, too
version = 1.0; //technically not the first version, but...
//an empty line here is recommended for clarity but the parser doesn't actually care

width=<int32>; //check the engine spec for limitations
height=<int32>; //check the engine spec for limitations
startx=<int32>; //player start, should be within the map grid
starty=<int32>; //player start, should be within the map grid
background="<string>"; //see the background spec for details
music="<string>"; //see the music spec for details
name="<string>"; //the human-readable name of the map, optional

//TILE DEFINITION
tile
{
	x=<integer>; //should be within the map grid
	y=<integer>; //should be within the map grid
	image=<"string">; //references some texture in the engine
	layer=<integer>; //see layer reference; 2 is not recommended
	blocking=<boolean>; //whether this is solid or not
}

//OBJECT DEFINITION
object
{
	x=<integer>; //should be within the map grid
	y=<integer>; //should be within the map grid
	facing=<direction>; //direction it is facing, default is no facing/toward
	id=<integer>; //optional; 0 means no unique ID
	thinker="<string>"; //refers to a thinker inside the engine
	param1="<string>"; //first thinker parameter, optional
	param2="<string>"; //second thinker parameter, optional
	param3="<string>"; //third thinker parameter, optional
	enabled=<boolean>; //whether this thinker is enabled or not (optional, default=true)
	solid=<boolean>; //whether this thinker is solid or not (optional, default=true)
	visible=<boolean>; //whether this thinker is visible or not (optional, default=true)
	layer=<integer>; //dependent on the engine actually supporting it; just use 2 for now
}
